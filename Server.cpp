#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>

using namespace std;

int main(){

    // Create Socket
    int listening = socket(AF_INET, SOCK_STREAM, 0);
    if(listening == -1){
        cerr<<"Failed to create socket";
        return -1;
    }

    // Bind Socket to IP/PORT
    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(54000); // HostTONetworkShort
    inet_pton(AF_INET, "0.0.0.0", &hint.sin_addr); // IP to number

    if(bind(listening, (sockaddr*)&hint, sizeof(hint)) == -1){
        cerr << "Failed to bind to IP/PORT";
        return -2;
    }

    cout << "Listening on "<<"0.0.0.0"<<" on port "<<"54000"<<endl;

    // Mark socket for listening
    if(listen(listening, SOMAXCONN) == -1){
        cerr<<"Failed to listen";
        return -3;
    }
    
    // Accept call
    sockaddr_in client;
    socklen_t clientSize = sizeof(client);
    char host[NI_MAXHOST];
    char svc[NI_MAXSERV];

    int clientSocket = accept(listening, (sockaddr*)&client, &clientSize);

    if(clientSocket == -1){
        cerr << "Problem with client connection";
        return -4;
    }

    // Close listening socket
    close(listening);

    memset(host, 0, NI_MAXHOST);
    memset(svc, 0, NI_MAXSERV);

    int result = getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, svc, NI_MAXSERV, 0);

    if(result){
        cout<<host<<" connected on "<<svc<<endl;
    } else {
        inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
        cout<<host<<" connected on "<<ntohs(client.sin_port)<<endl;
    }

    // Display received message
    char buf[4096];
    while(true){
        // CLear buffer
        memset(buf, 0, 4096);

        // Wait for message
        int bytesRecv = recv(clientSocket, buf, 4096, 0); // recieve the message
        if(bytesRecv == -1){
            cerr << "Connection issue"<<endl;
            break;
        }

        if(bytesRecv == 0){
            cout<<"Client Disconnected"<<endl;
            break;
        }

        // Display message
        string recv(buf, 0, bytesRecv);
        cout<<"Received: "<<recv<<endl;

        if(string(buf, 0, bytesRecv) == "exit\r\n"){
            break;
        }

        // Resend message
        // send(clientSocket, buf, bytesRecv + 1, 0);
        send(clientSocket, string("> "+recv).c_str(), bytesRecv + 2, 0);
    }
    
    // Close Socket
    close(clientSocket);
    return 0;
}
