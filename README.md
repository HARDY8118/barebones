# Bare Bones CPP Server/Client

## Connect to server using telnet

```bash
telnet localhost 54000
```

## Quit telnet
Make note of `Escape character is '^]'` when connected to server.

Use <kbd>Ctrl</kbd> to send `^`.

For example <kbd>Ctrl</kbd> + <kbd>X</kbd> to send `^X`.

So for `^]` use <kbd>Ctrl</kbd> + <kbd>]</kbd>, this will enter in telnet command mode. Enter `quit` to disconnect.
