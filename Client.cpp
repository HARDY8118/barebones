#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>

using namespace std;

int main(){

    // Create socket
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1){
        cerr << "Failed to create socket";
        return 1;
    }

    // Create hint structure for server
    int port = 54000;
    string ipAddress = "127.0.0.1";

    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(port);
    inet_pton(AF_INET, ipAddress.c_str(), &hint.sin_addr);

    // Connect to server
    int connectRes = connect(sock, (sockaddr*)&hint, sizeof(hint));
    if(connectRes == -1){
        cerr << "Failed to connect";
        return 1;
    }

    // Loop
    char buf[5096];
    string userInput;

    do{
        // Enter lines
        cout<<"> ";
        getline(cin, userInput);

        // Send to server
        int sendRes = send(sock, userInput.c_str(), userInput.size()+1, 0);
        if(sendRes == -1){
            cerr << "Failed to send to server";
            cout << "Failed to send to server\r\n";
            continue;
        }

        // Wait for response
        memset(buf, 0, 4096);
        int bytesReceived = recv(sock, buf, 4096, 0);
        if(bytesReceived == -1){
            cout << "Error getting response";
        } else {
            // Display response
            cout << "SERVER"<<string(buf, bytesReceived)<<"\r\n";
        }
    }while(true);

    // Close Socket
    close(sock);

    return 0;

}
